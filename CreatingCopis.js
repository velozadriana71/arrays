const testResults = [1, 5.3, 1.5, 10.99, -5, 10]
//const storedResults = testResults.slice(0,2); //Seleccionar un rango
const storedResults = testResults.concat([3.99, 2]);

testResults.push(5.91);

//Slice devuelve una matriz nueva, puede seleccionar los elementos que se necesiten
console.log(storedResults, testResults);
//Retorna el indice del elemento seleccionado no funciona con objetos
console.log(testResults.indexOf(1.5));
//Retornar un valor que se busca por la derecha no funciona con objetos
console.log(testResults.lastIndexOf(1.5));

//Si incluye un valor la matriz con el siguiente elemento retorna un booleano
console.log(testResults.includes(10.99));


const personData = [{name: 'Adriana'}, {name: 'Carmen'}];
console.log(personData.indexOf({name: 'Carmen'}));

//puede aceptar 3 argumentos el primero es un objeto de la matriz, el segundo el indice y el tercero
//el objeto espera que retorne un booleano true para el que se busca false para el resto
const carmen = personData.find((person, idx, persons) => {
    return person.name === 'Carmen'; //Regresa el mismo objeto de la matriz
}) 
carmen.name = 'Veloz';

console.log(carmen, personData);

//Encontrar el indice del elemento que se esta buscando
const adriIndex = personData.findIndex((person, idx, persons) => {
    return person.name === 'Carmen'; 
}) 
console.log(adriIndex);