const data = 'new york; 10.99; 2000';

//Divide una cadena en una matriz de multiples segmentos y como argumento pasa
// el separador por el cual divide.
const transformedData = data.split(';');
console.log(transformedData);

const nameFragements = ['Adriana', 'Veloz'];
//El método join une los elementos de una matriz en una cadena
//Puede retornar un elemento vacio para que no sea separado por una coma
const names = nameFragements.join(' ');
console.log(names);

//Spread operator Es un operador que extrae todos los elementos de una matriz y 
//se los proporciona a una lista independiente de elementos.

const copiedNameFragments = [...nameFragements];
nameFragements.push('Miss');
console.log(nameFragements, copiedNameFragments);

const prices = [10.99, 5.99, 3.99, 6.59];
const tax = 0.19;
//Min metodo toma un par de argumentos, NO toma una matriz y después devuelve el valor mas pequeño
console.log(Math.min(...prices));
// console.log(Math.min(1, 5, -3));

const persons = [{ name: 'Adri', age: 23}, {name: 'Carmen', age: 24 }];
const copiedPersons = persons.map(person => ({
    name: person.name,
    age: person.age
}));

persons.push({ name: 'Andrea', age: 64 });
persons[0].age = 24;
console.log(persons, copiedPersons);