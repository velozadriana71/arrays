// Forma tradicional y que siempre se usa para crear una matriz
const numbers = [1, 2, 3, 4];
console.log(numbers);

// // Función constructora (La nueva mátriz construye una nueva matriz)
// const moreNumbers = new Array('Hi', 'Welcome'); // []
// console.log(moreNumbers);

// //
// const yetMoreNumbers = Array.of(1, 2);
// console.log(yetMoreNumbers);

const listItems = document.querySelectorAll('li');
console.log(listItems);

//Permite convertir un objeto iterable a una matriz
const arrayListItem = Array.from(listItems); //["H", "I"] Los separa por caracteres
console.log(arrayListItem);

const hobbies = ['Cooking', 'Sports'];
const personalData = [23, 'Adri', {moreDetail: []}]
console.log(personalData[1]);

const analyticsData = [[1, 1.6], [-5.4, 2.1]];

for(const data of analyticsData) {
    for(const dataPoin of data) {
        console.log(dataPoin);
    }
}
