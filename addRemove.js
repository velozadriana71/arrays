const hobbies = ['Sports', 'Cooking'];

//AGREGAR NUEVOS ELEMENTOS
hobbies.push('Reading');
// Agregar un nuevo elemento a la matriz al inicio (Mueve los elementos a la deracha)
hobbies.unshift('Coding');

//ELIMINAR ELEMENTOS
const poppedValue = hobbies.pop();
//Eliminar el primer elemento (Mueve los elementos a la izquierda).
hobbies.shift();

console.log(hobbies)

//Reemplazar elementos en cualquier parte de la matriz
hobbies[1] = 'Coding';
// hobbies[5] = 'Reading'; //['Sports', 'Cooking' '', '','','Reading'];
console.log(hobbies);

//Agragar entre dos elementos Primer elemento posisición segundo no quiero eliminar nada
//Tercer elemento lo que se quiere agregar.
hobbies.splice(2, 0, 'Good Food'); 
console.log(hobbies);

//Ejemplo de eliminar un elemento con splice
hobbies.splice(0,1);
console.log(hobbies);