const nameData = ["Adriana", "Veloz", 'Miss', 23];
// const firstName = nameData[0];
// const lastName = nameData[1];


//Esta sintaxis de destructuración se lee y se asigna de izquierda a derecha
//Se usa si se desea dividir una matriz en variables o constantes que despues se
//Puede usar en cualquier parte del código
const [firstName, lastName, ...otherInfo] = nameData;
console.log(firstName, lastName, otherInfo);