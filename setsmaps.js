//Set son estructuras de datos que ayudan administrar valores unicos 

const ids = new Set(["Hi", "Adri", "Veloz"]);
//Agregar
ids.add(2);

//Eliminar
if (ids.has("Hi")) {
  ids.delete("Hi");
}

//Retorna true o false
// console.log(ids.has(1));

//Entries devuelve un iterable de matrices donde siempre tiene dos entradas
//Devuelve dos entradas para estar en línea con el metodo maps
for (const entry of ids.entries()) {
 // console.log(entry);
}


//Maps

const person1 = { name: 'Adri' };
const person2 = { name: 'Veloz' };

//Se puede inicializar con una matriz de matrices [[]], porque cada matriz es un par
// clave-valor la clave y el valor puede ser de cualquier tipo string, number, object
const personData = new Map([[person1, [{date: 'yesterday', price: 10}]]]);

personData.set(person2, [{date: 'two weeks ago', price:100}]);

console.log(personData);
console.log(personData.get(person1));

//Se registra una matriz con exactamente dos elementos el primero es la clave (key)
//el segundo elemento es el valor (value)
for(const entry of personData.entries()) {
    console.log(entry);
}

for(const [key, value] of personData.entries()) {
    console.log(key, value)
}

for (const key of personData.keys()) {
    console.log(key)
}

for (const value of personData.values()) {
    console.log(value)
}


//WeakSet funciona internamente de tal manera que solo puede almacenar objetos
//Permite que la recoleccion de basura elimine elementos que son parte del conjunto que no 
//se utiliza en otra parte del codigo
let person = {name: 'Adri'};
const persons = new WeakSet();
persons.add(person);

//Operaciones
// person = null;

console.log(persons);

const personalData = new WeakMap();
personalData.set(person, 'Extra info');

person = null