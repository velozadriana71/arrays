// const prices = [10.99, 5.99, 3.99, 6.59];
// const tax = 0.19;
// const taxAdjustedPrices = [];

// // for(const price of prices) {
// //     taxAdjustedPrices.push(price + (1 * tax));
// // }

// //El método forEach toma una función y esa función toma 3 argumentos. Se ejecuta por cada
// //elemento de la matriz, primer elemento precio, segundo indice y por ultimo la matriz completa
// prices.forEach((price, idx, prices) => {
//     const priceObj = { index: idx, taxAdjustedPrices: price * (1 + tax) };
//     taxAdjustedPrices.push(priceObj);
// });

// console.log(taxAdjustedPrices);

const prices = [10.99, 5.99, 3.99, 6.59];
const tax = 0.19;

//Toma la matriz, ejecuta una funcion que debería devolver un nuevo elemento para cada elemento 
//Un elemento posible transformado
const taxAdjustedPrices = prices.map((price, idx, prices) => {
    const priceObj = { index: idx, taxAdjustedPrices: price * (1 + tax) };
    return priceObj;
});

// console.log(prices, taxAdjustedPrices);

//Ordena por defecto convierte todo en una cadena y luego ordena esto en una lógica de cadena
//
const sortedPrices = prices.sort((a,b) => {
    if(a > b) {
        return 1; //Reverse -1
    } else if (a === b) {
        return 0
    } else {
        return -1 //Reverse 1
    }
});
console.log(sortedPrices);

//No se puede pasar ningún argumento a reverse simplemente invierte en una matriz
console.log(sortedPrices.reverse());

//Retorna un booleano. Filtra una matriz
const filteredArray = prices.filter((price, index, prices) => {
    return price > 6;
})

//COMO SE PUEDE REDUCIR PORQUE SE ESTA USANDO LA FUNCIÓN FLECHA.
// const filteredArray = prices.filter(price => price > 6)
console.log(filteredArray);

// let suma = 0;
// prices.forEach((price) =>{
//     suma += price
// })

//REDUCE: Reduce una matriz a un valor más simple
//Toma un valor previo,un valor actual, un indice y opcional la matriz original
const sum = prices.reduce((prevValue, curValue, curIndex, prices) => {
    return prevValue + curValue;
}, 0); //Segundo valor con el que se decia iniciar
console.log(sum);